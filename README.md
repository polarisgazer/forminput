# FormInput

[![CI Status](https://img.shields.io/travis/Undisclosed/FormInput.svg?style=flat)](https://travis-ci.org/Undisclosed/FormInput)
[![Version](https://img.shields.io/cocoapods/v/FormInput.svg?style=flat)](https://cocoapods.org/pods/FormInput)
[![License](https://img.shields.io/cocoapods/l/FormInput.svg?style=flat)](https://cocoapods.org/pods/FormInput)
[![Platform](https://img.shields.io/cocoapods/p/FormInput.svg?style=flat)](https://cocoapods.org/pods/FormInput)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

FormInput is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'FormInput'
```

## Author

Undisclosed, undisclosed@email.com

## License

FormInput is available under the MIT license. See the LICENSE file for more info.
